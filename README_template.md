# Git-Tools

Tools to help maintain Perl 6 modules within a Git repository

# Version

VERSION-PLACEHOLDER

# Synopsis

Use git as normal up until the time that you want to commit your changes. Then use the `commit` utility which will capture your Git commit message and include it in your CHANGES file.

    git add file

    commit 'message'

# Dependencies:  

Rakudo Star

# CHANGES

