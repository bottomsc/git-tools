# Git-Tools

Tools to help maintain Perl 6 modules within a Git repository

# Version

0.0.5

# Synopsis

Use git as normal up until the time that you want to commit your changes. Then use the `commit` utility which will capture your Git commit message and include it in your CHANGES file.

    git add file

    commit 'message'

# Dependencies:  

Rakudo Star

# CHANGES

0.0.5: Improved formatting of CHANGE log  
0.0.4: Added README template to repository  
0.0.3: Added README  
